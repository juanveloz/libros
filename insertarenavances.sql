insert into avances (
   idavance
  ,fecha
  ,numeroprogreso
  ,fkisbn
) VALUES (
   NULL -- idavance - IN int(11)
  ,'20 de noviembre 2000'  -- fecha - IN varchar(100)
  ,23   -- numeroprogreso - IN int(11)
  ,1   -- fkisbn - IN int(11)
);
insert into avances (
   idavance
  ,fecha
  ,numeroprogreso
  ,fkisbn
) VALUES (
   NULL -- idavance - IN int(11)
  ,'1 de enero 1989'  -- fecha - IN varchar(100)
  ,10   -- numeroprogreso - IN int(11)
  ,2  -- fkisbn - IN int(11)
);
select * from avances;